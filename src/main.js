import Vue from 'vue';
import App from './App.vue';
import VModal from 'vue-js-modal';
import VeeValidate from 'vee-validate';
import download from './modules/downloading';
import api from './modules/api';
import cy from './modules/cytoscape';

import router from './modules/router';
import store from './modules/store';

import 'bootswatch/dist/cerulean/bootstrap.min.css';


Vue.config.productionTip = false;

Vue.use(VModal, {dynamic: true, injectModalsContainer: true, dynamicDefaults: {clickToClose: false}});
Vue.use(VeeValidate);

new Vue({
    router,
    store,
    download,
    api,
    cy,
    render: h => h(App)
}).$mount('#app');
