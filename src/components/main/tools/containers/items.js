class Host {
    image = 'img/host.svg';

    constructor(name) {
        this.name = name;
    }
}

class Switch {
    image = 'img/router-0.svg';

    constructor(name) {
        this.name = name;
    }
}

export { Host, Switch }