const Metric = Object.freeze({
    DELAY: 'delay',
    BANDWIDTH: 'bandwidth',
    PACKET_LOSS_RATE: 'packetLossRate',
    ADMIN_METRIC: 'adminMetric',
    LARAC: 'larac',
    LARAC_COST: 'laracCost',
    QOS_PARAMS: 'qosParams',
});

export {Metric};