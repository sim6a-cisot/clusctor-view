const bisectionStore = {
    state: {
        bisection: {
            parts: 2
        }
    },

    getters: {
        BISECTION_PARTS: state => {
            return state.bisection.parts;
        }
    },

    mutations: {
        SET_BISECTION_PARTS: (state, parts) => {
            state.bisection.parts = parts;
        }
    }
};

export {bisectionStore};
