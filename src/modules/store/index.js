import Vuex from 'vuex';
import Vue from 'vue';
import {toolStore} from './tool';
import {segmentationStore} from './segmentation';
import {bisectionStore} from './bisection';
import {statisticsStore} from './statistics';
import {topologyStore} from './topology';
import {flowSettingsStore} from "./flowSettings";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: Object.assign({},
        toolStore.state,
        segmentationStore.state,
        bisectionStore.state,
        topologyStore.state,
        statisticsStore.state,
        flowSettingsStore.state,
    ),

    getters: Object.assign({},
        toolStore.getters,
        segmentationStore.getters,
        bisectionStore.getters,
        topologyStore.getters,
        statisticsStore.getters,
        flowSettingsStore.getters,
    ),

    mutations: Object.assign({},
        toolStore.mutations,
        segmentationStore.mutations,
        bisectionStore.mutations,
        topologyStore.mutations,
        statisticsStore.mutations,
        flowSettingsStore.mutations,
    ),

    actions: {},
});

export default store;
