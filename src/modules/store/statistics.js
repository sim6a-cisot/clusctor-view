import { Tool } from '../tool';

const statisticsStore = {
    state: {
        statistics: {
            algorithm: Tool.SEGMENTATION
        }
    },

    getters: {
        STATISTICS_ALGORITHM: state => {
            return state.statistics.algorithm;
        }
    },

    mutations: {
        SET_STATISTICS_ALGORITHM: (state, algorithm) => {
            state.statistics.algorithm = algorithm;
        }
    }
};

export {statisticsStore};
