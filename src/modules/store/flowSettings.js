import {FlowSettings} from '../flowSettings';

const flowSettingsStore = {
    state: {
        flow: new FlowSettings()
    },

    getters: {
        FLOW_SETTINGS: state => {
            return state.flow;
        }
    },

    mutations: {
        SET_FLOW_SETTINGS: (state, flow) => {
            state.flow = flow;
        }
    }
};

export {flowSettingsStore};