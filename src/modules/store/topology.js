const topologyStore = {
    state: {
        topology: {
            state: [],
            isInitialized: false
        }
    },

    getters: {
        TOPOLOGY_STATE: state => state.topology.state,
        TOPOLOGY_IS_INITIALIZED: state => state.topology.isInitialized
    },

    mutations: {
        SET_TOPOLOGY_STATE: (state, topologyState) => {
            state.topology.state = topologyState;
        },
        SET_TOPOLOGY_IS_INITIALIZED: (state, isInitialized) => {
            state.topology.isInitialized = isInitialized;
        }
    }
};

export {topologyStore}
