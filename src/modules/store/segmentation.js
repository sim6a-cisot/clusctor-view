import { SegmentationStep } from '../segmentation';

const segmentationStore = {
    state: {
        segmentation: {
            connectivity: 0.3,
            step: SegmentationStep.PRIMARY_SEGMENTS
        }
    },

    getters: {
        SEGMENTATION_CONNECTIVITY: state => {
            return state.segmentation.connectivity;
        },
        SEGMENTATION_STEP: state => {
            return state.segmentation.step;
        }
    },

    mutations: {
        SET_SEGMENTATION_CONNECTIVITY: (state, connectivity) => {
            state.segmentation.connectivity = connectivity;
        },
        SET_SEGMENTATION_STEP: (state, step) => {
            state.segmentation.step = step;
        }
    }
};

export {segmentationStore};
