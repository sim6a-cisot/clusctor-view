import { Tool } from '../tool';

const toolStore = {
    state: {
        tool: Tool.SEGMENTATION
    },

    getters: {
        TOOL: state => {
            return state.tool;
        }
    },

    mutations: {
        SET_TOOL: (state, tool) => {
            state.tool = tool;
        }
    }
};

export { toolStore };
