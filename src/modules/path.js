
class Path {
    constructor(srcHost, dstHost, switchSequence) {
        this.srcHost = srcHost;
        this.dstHost = dstHost;
        this.switchSequence = switchSequence;
        this.srcIp = '';
        this.dstIp = '';
        this.priority = 0;
        this.idleTimeout = 0;
        this.hardTimeout = 0;
        this.ipProto = '';
        this.ethType = '';
        this.servicePort = 0;
    }

    setTimeouts(idle, hard) {
        this.idleTimeout = parseInt(idle);
        this.hardTimeout = parseInt(hard);
    }

    setPriority(p) {
        this.priority = parseInt(p);
    }

    setServiceProperties(ipProto, port) {
        this.ipProto = ipProto;
        if (port === '') {
            port = 0;
        }
        this.servicePort = parseInt(port);
    }

    setEthType(t) {
        this.ethType = t
    }
}

export { Path };