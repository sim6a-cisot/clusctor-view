const TrafficType = Object.freeze({
    VIDEO_STREAM: 'videoStream',
    FILE_EXCHANGE: 'fileExchange',
    EMAIL: 'email',
    VOIP: 'voip',
    E_COMMERCE: 'eCommerce'
});

class Traffic {
    constructor(type, name) {
        this.name = name;
        this.type = type;
    }
}

class Collection {
    constructor(traffics) {
        this.traffics = traffics;
    }
}

export {TrafficType, Traffic, Collection}