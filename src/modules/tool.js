export const Tool = Object.freeze({
    PRIM: 'primAlg',
    SEGMENTATION: 'e-segmentation',
    SLICING: 'qosSlicing',
    BISECTION: 'bisection',
    DIJKSTRA: 'dijkstraAlg',
    GIRVAN_NEWMAN: 'girvanNewmanAlg',
    CUSTOM_ROUTING: 'customRouting',
    TOPOLOGY_GENERATOR: 'networkGenerator'
});