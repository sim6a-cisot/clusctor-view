
class OFSwitch {
    constructor(name, dpid) {
        this.name = name;
        this.dpid = dpid;
    }
}

class Collection {
    constructor(switches) {
        this.switches = switches;
    }

    findDPIDByName(name) {
        const filtered = this.switches.filter(sw => sw.name === name);
        if (filtered.length !== 1) {
            return '';
        }
        return filtered[0].dpid
    }

    createDPIDArray(names) {
        const arr = [];
        for (let i = 0; i < names.length; i++) {
            arr.push(this.findDPIDByName(names[i]));
        }
        return arr;
    }
}

export { OFSwitch, Collection };