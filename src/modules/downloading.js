import Vue from 'vue';

Vue.mixin({
    beforeCreate() {
        const options = this.$options;
        if (options.download) {
            this.$download = options.download;
        }
        else if (options.parent && options.parent.$download) {
            this.$download = options.parent.$download;
        }
    }
});

class Downloader {
    png(data, fileName) {
        download(data, fileName, "image/png");
    }

    csv(data, fileName) {
        download(data, fileName, "text/csv");
    }
}

function download(data, fileName, mimeType) {
    const blob = new Blob([data], {type: mimeType});
    const url = window.URL.createObjectURL(blob);
    const downloadLink = document.createElement("a");
    downloadLink.href = url;
    downloadLink.download = fileName;
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
}

const downloader = new Downloader();

export default downloader;