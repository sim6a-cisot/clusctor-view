import axios from 'axios';

class Slicing {
    run(sliceDescriptions, callback) {
        axios
            .post(`api/v1/qos-slicer/slices`, sliceDescriptions)
            .then(callback);
    }
    showSlice(trafficType, callback) {
        axios
            .put(`api/v1/qos-slicer/slices?trafficType=${trafficType}`)
            .then(callback);
    }
    deleteSlices(callback) {
        axios
            .delete(`api/v1/qos-slicer/slices`)
            .then(callback);
    }
}

export default Slicing;