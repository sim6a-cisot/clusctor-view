import axios from 'axios';

class Dijkstra {
    install(path) {
        axios
            .post(`/api/v1/dijkstra/path`, path)
            .finally();
    }

    show(srcMac, dstMac, callback) {
        axios
            .get(`/api/v1/dijkstra/path?srcmac=${srcMac}&dstmac=${dstMac}`)
            .then(callback);
    }
}

export default Dijkstra;