import axios from 'axios';

class Bisection {
    run(parts, callback) {
        axios
            .post(`api/v1/bisection/segments?parts=${parts}`)
            .then(callback);
    }

    analyse(parts, callback) {
        axios
            .get(`/api/v1/bisection/analysis/stats?parts=${parts}`)
            .then(callback);
    }
}

export default Bisection;