import axios from 'axios';

class Metric {
    get(callback) {
        axios
            .get(`api/v1/topology/metric`)
            .then(callback);
    }

    save(value, useCustomMetrics, callback) {
        axios
            .put(`api/v1/topology/metric?value=${value}&useCustomMetrics=${useCustomMetrics}`)
            .then(callback);
    }
}

export default Metric;
