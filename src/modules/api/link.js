import axios from 'axios';

class Link {
    save({sourceDPID, destinationDPID, delay, bandwidth, packetLossRate, adminMetric}, callback) {
        axios
            .put(`api/v1/topology/link?srcdpid=${sourceDPID}&dstdpid=${destinationDPID}`+
                `&delay=${delay}&bandwidth=${bandwidth}&packetLossRate=${packetLossRate}`+
                `&adminMetric=${adminMetric}`)
            .then(callback);
    }
}

export default Link;
