import axios from 'axios';

class Segmentation {
    run(q, step, callback) {
        axios
            .post(`api/v1/e-segmenter/segments?q=${q}&step=${step}`)
            .then(callback);
    }

    analyse(q, callback) {
        axios
            .get(`/api/v1/e-segmenter/analysis/stats?q=${q}`)
            .then(callback);
    }

    clear(callback) {
        axios
            .delete(`api/v1/topology/segments`)
            .then(callback);
    }
}

export default Segmentation;
