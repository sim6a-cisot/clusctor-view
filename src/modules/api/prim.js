import axios from 'axios';

class Prim {
    run(callback) {
        axios
            .post(`api/v1/prim/tree`)
            .then(callback);
    }
}

export default Prim;