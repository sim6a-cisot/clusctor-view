import axios from 'axios';

class Topology {
    get(callback) {
        axios
            .get(`api/v1/topology`)
            .then(callback);
    }

    save() {
        axios
            .get(`api/v1/topology?save=json`)
            .finally()
    }

    generate(switchesQuantity, randomLinksQuantity, hostsQuantity, topologiesQuantity) {
        axios
            .post(`api/v1/topology?switches=${switchesQuantity}&randomLinks=${randomLinksQuantity}&hosts=${hostsQuantity}&topologies=${topologiesQuantity}`)
            .finally()
    }
}

export default Topology;
