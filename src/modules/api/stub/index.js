import Topology from "./topology";

class Api {
    constructor(topology) {
        this.topology = topology;
    }
}

const api = new Api(
    new Topology(),
);

export default api;