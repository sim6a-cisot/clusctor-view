const elements = [
    {
        data: {
            id: 'S1',
            dpid: '00:00:00:00:00:00:00:01',
            segmentId: '1',
            isSwitch: true,
        },
    },
    {
        data: {
            id: 'S2',
            dpid: '00:00:00:00:00:00:00:02',
            segmentId: '1',
            isSwitch: true,
        },
    },
    {
        data: {
            id: 'S3',
            dpid: '00:00:00:00:00:00:00:03',
            segmentId: '0',
            isSwitch: true,
        }
    },
    {
        data: {
            id: 'S1-S2',
            source: 'S1',
            target: 'S2',
            srcdpid: '00:00:00:00:00:00:00:01',
            dstdpid: '00:00:00:00:00:00:00:02',
            label: '15 Bit/s',
            bandwidth: 15,
            delay: 1,
            packetLossRate: 20,
            adminMetric: 12,
            isSwitchLink: true,
        }
    },
    {
        data: {
            id: 'S2-S3',
            source: 'S2',
            target: 'S3',
            srcdpid: '00:00:00:00:00:00:00:02',
            dstdpid: '00:00:00:00:00:00:00:03',
            label: '200 Mbit/s',
            bandwidth: 200,
            delay: 1,
            packetLossRate: 20,
            adminMetric: 12,
            isSwitchLink: true,
        }
    },
    {
        data: {
            id: 'S3-S1',
            source: 'S3',
            target: 'S1',
            srcdpid: '00:00:00:00:00:00:00:03',
            dstdpid: '00:00:00:00:00:00:00:01',
            label: '1',
            bandwidth: 300,
            delay: 1,
            packetLossRate: 20,
            adminMetric: 12,
            isSwitchLink: true,
        }
    },

    {
        data: {
            id: 'H1',
            ip: '10.0.0.1',
            mac: '00:00:00:00:00:01',
            isHost: true,
        }
    },
    {
        data: {
            id: 'H2',
            ip: '10.0.0.2',
            mac: '00:00:00:00:00:02',
            isHost: true,
        }
    },
    {
        data: {
            id: 'H3',
            ip: '10.0.0.3',
            mac: '00:00:00:00:00:03',
            isHost: true,
        }
    },
    {
        data: {
            id: 'H1-S1',
            source: 'H1',
            target: 'S1',
            isHostLink: true,
        }
    },
    {
        data: {
            id: 'H2-S2',
            source: 'H2',
            target: 'S2',
            isHostLink: true,
        }
    },
    {
        data: {
            id: 'H3-S3',
            source: 'H3',
            target: 'S3',
            isHostLink: true,
        }
    },
];

class Topology {
    get(callback) {
        callback({data: elements});
    }
    save() {}
}

export default Topology;
