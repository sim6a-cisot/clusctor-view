import axios from 'axios';

class Tree {
    clear(callback) {
        axios
            .delete(`api/v1/topology/tree`)
            .then(callback);
    }
}

export default Tree;