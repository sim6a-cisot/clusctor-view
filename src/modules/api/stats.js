import axios from "axios";


class Stats {
    segmentation(callback) {
        axios
            .get(`api/v1/e-segmenter/segments/stats`)
            .then(callback);
    }

    segmentationCsv(callback) {
        axios
            .get(`api/v1/e-segmenter/segments/stats?export=csv`)
            .then(callback);
    }

    bisection(callback) {
        axios
            .get(`api/v1/bisection/segments/stats`)
            .then(callback);
    }

    bisectionCsv(callback) {
        axios
            .get(`api/v1/bisection/segments/stats?export=csv`)
            .then(callback);
    }

    slicing(callback) {
        axios
            .get(`api/v1/qos-slicer/slices/stats`)
            .then(callback);
    }

    slicingCsv(callback) {
        axios
            .get(`api/v1/qos-slicer/slices/stats?export=csv`)
            .then(callback);
    }

    prim(callback) {
        axios
            .get(`api/v1/prim/tree/stats`)
            .then(callback);
    }

    primCsv(callback) {
        axios
            .get(`api/v1/prim/tree/stats?export=csv`)
            .then(callback);
    }

    girvanNewman(callback) {
        axios
            .get(`api/v1/girvan-newman/segments/stats`)
            .then(callback);
    }

    girvanNewmanCsv(callback) {
        axios
            .get(`api/v1/girvan-newman/segments/stats?export=csv`)
            .then(callback);
    }
}

export default Stats;