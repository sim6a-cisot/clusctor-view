import axios from 'axios';

class Path {
    install(path) {
        axios
            .post(`/api/v1/path`, path)
            .finally();
    }

    update(path) {
        this.install(path);
    }

    visualize(path, callback) {
        axios
            .post(`api/v1/path/view`, path)
            .then(callback);
    }

    getAll(callback) {
        axios
            .get(`/api/v1/path`)
            .then(callback);
    }
}

export default Path;