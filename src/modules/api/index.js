import Topology from './topology';
import Segmentation from './segmentation';
import Metric from './metric';
import Link from './link';
import Prim from './prim';
import Dijkstra from "./dijkstra";
import Tree from "./tree";
import Stats from './stats';
import Slicing from './slicing';
import Bisection from './bisection';
import GirvanNewman from "./girvanNewman";
import Path from "./path";
import Vue from 'vue';

Vue.mixin({
    beforeCreate() {
        const options = this.$options;
        if (options.api) {
            this.$api = options.api;
        } else if (options.parent && options.parent.$api) {
            this.$api = options.parent.$api;
        }
    }
});


class Api {
    constructor(topology, segmentation, bisection, slicing,
                metric, link, prim, dijkstra, girvanNewman, stats, tree, path) {
        this.topology = topology;
        this.segmentation = segmentation;
        this.bisection = bisection;
        this.slicing = slicing;
        this.metric = metric;
        this.link = link;
        this.prim = prim;
        this.dijkstra = dijkstra;
        this.girvanNewman = girvanNewman,
        this.stats = stats;
        this.tree = tree;
        this.path = path;
    }
}

const api = new Api(
    new Topology(),
    new Segmentation(),
    new Bisection(),
    new Slicing(),
    new Metric(),
    new Link(),
    new Prim(),
    new Dijkstra(),
    new GirvanNewman(),
    new Stats(),
    new Tree(),
    new Path()
);

export default api;
