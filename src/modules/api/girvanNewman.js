import axios from 'axios';

class GirvanNewman {
    run(callback) {
        axios
            .post(`api/v1/girvan-newman/segments`)
            .then(callback);
    }

    analyse(callback) {
        axios
            .get(`/api/v1/girvan-newman/analysis/stats`)
            .then(callback);
    }
}

export default GirvanNewman;