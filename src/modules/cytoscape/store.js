import store from '../store';


class Store {
    constructor() {
        this.store = store;
    }

    isInitialized() {
        return this.store.getters.TOPOLOGY_IS_INITIALIZED;
    }

    initialize() {
        this.store.commit('SET_TOPOLOGY_IS_INITIALIZED', true);
    }

    get topology() {
        return this.store.getters.TOPOLOGY_STATE;
    }

    set topology(topologyJson) {
        this.store.commit('SET_TOPOLOGY_STATE', topologyJson);
    }
}

export default Store;
