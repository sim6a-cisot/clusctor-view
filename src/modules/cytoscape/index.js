import holder from './holder';
import Vue from 'vue';

Vue.mixin({
    beforeCreate() {
        const options = this.$options;
        if (options.cy) {
            this.$cy = options.cy;
        }
        else if (options.parent && options.parent.$cy) {
            this.$cy = options.parent.$cy;
        }
    }
});

export default holder;
