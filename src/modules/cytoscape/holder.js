import cytoscape from 'cytoscape';
import cola from 'cytoscape-cola';
import api from '../api';
// import api from '../api/stub';

import Store from './store';
import { Host, Collection as HostCollection } from "../host";
import { OFSwitch, Collection as SwitchCollection } from "../switch";

const style = [
    {
        selector: 'node',
        style: {
            'label': 'data(id)',
            'font-family': 'cambria',
            'font-size': 12,
            'background-opacity': 0,
            'border-opacity': 0,
        }
    },

    {
        selector: 'node[?isSwitch]',
        style: {
            'background-image': (e) => `img/router-${e.data().segmentId}.svg`,
            'height': 35,
            'width': 45,
            'shape': 'round-rectangle'
        }
    },

    {
        selector: 'node[?isHost]',
        style: {
            'background-image': 'img/host.svg',
            'height': 40,
            'width': 30,
            'shape': 'round-rectangle',
            'border-width': 5,
        }
    },

    {
        selector: 'edge',
        style: {
            'line-color': 'black',
            'width': 1,
            'source-text-offset': 30,
        }
    },

    {
        selector: 'edge[?isSwitchLink]',
        style: {
            'font-family': 'cambria',
            'font-size': 12,

            'label': 'data(label)',

            'text-background-color': 'white',
            'text-background-opacity': 1,
        }
    },

    {
        selector: '.inRoute',
        style: {
            'line-color': 'red',
            'width': 4,
        }
    },

    {
        selector: 'node[?isSwitch].selected',
        style: {
            'border-opacity': 0.5,
            'border-style': 'solid',
            'border-color': 'red',
            'border-width': 4
        }
    },

    {
        selector: 'node[?isHost].selected',
        style: {
            'border-opacity': 0.5,
            'border-style': 'solid',
            'border-color': 'green',
            'border-width': 4
        }
    },

    {
        selector: '.inTree',
        style: {
            'line-color': 'black',
            'width': 4,
        }
    },

    {
        selector: 'edge.invisible',
        style: {
            'opacity': 0.1
        }
    },

    {
        selector: 'node.invisible',
        style: {
            'opacity': 0.4
        }
    }
];

class Holder {
    constructor(store, api) {
        this.store = store;
        this.api = api;
        this.cy = null;
    }

    load(container, linkTapHandlerFunc, switchTapHandlerFunc, hostTapHandlerFunc) {
        const isStoreInitialized = this.store.isInitialized();

        if (!isStoreInitialized) {
            this.store.initialize();
            cytoscape.use(cola);
        }

        this.cy = cytoscape({
            container
        });

        this.cy.json(this.store.topology);
        this.cy.json({style});

        this.handleLinkTap(linkTapHandlerFunc);
        this.handleSwitchTap(switchTapHandlerFunc);
        this.handleHostTap(hostTapHandlerFunc);

        if (isStoreInitialized) {
            this.update();
        } else {
            this.refresh();

            setInterval(() => {
                this.api.topology.get(response => {
                    this.removeElementClasses();
                    this.cy.json({elements: response.data});
                })
            }, 5000);
        }
    }

    save() {
        this.store.topology = this.cy.json();
    }

    update() {
        this.api.topology.get(response => {
            this.removeElementClasses();
            this.cy.json({elements: response.data});
        })
    }

    getHostCollection() {
        const hosts = this.cy.nodes('[?isHost]');
        const collectedHosts = [];
        let host;
        for (let i = 0; i < hosts.length; i++) {
            host = hosts[i].data();
            collectedHosts.push(new Host(host.id, host.mac, host.ip));
        }
        return new HostCollection(collectedHosts);
    }

    getSwitchCollection() {
        const switches = this.cy.nodes('[?isSwitch]');
        const collectedSwitches = [];
        let sw;
        for (let i = 0; i < switches.length; i++) {
            sw = switches[i].data();
            collectedSwitches.push(new OFSwitch(sw.id, sw.dpid));
        }
        return new SwitchCollection(collectedSwitches);
    }

    removeElementClasses() {
        this.cy.elements().classes([]);
    }

    createJpeg() {
        return this.cy.jpeg({
            output: 'blob',
            bg: 'white',
            full: true
        });
    }

    refresh() {
        this.api.topology.get(response => {
            this.cy.json({elements: response.data});
            this.cy.elements().layout({name: 'cola'}).run();
        })
    }

    handleHostTap(hostTapHandlerFunc) {
        this.cy.on('tap', 'node', (event) => {
            let node = event.target;
            if (node.data().isHost) {
                hostTapHandlerFunc(node.data());
            }
        });
    }

    handleSwitchTap(switchTapHandlerFunc) {
        this.cy.on('tap', 'node', (event) => {
            let node = event.target;
            if (node.data().isSwitch) {
                switchTapHandlerFunc(node.data());
            }
        });
    }

    handleLinkTap(linkTapHandlerFunc) {
        this.cy.on('tap', 'edge', (event) => {
            let edge = event.target;
            if (edge.data().isSwitchLink) {
                linkTapHandlerFunc(edge.data());
            }
        });
    }

    isPathExists(nodeIds) {
        for (let i = 0; i < nodeIds.length - 1; i++) {
            if (!this.isEdgeExists(nodeIds[i], nodeIds[i+1])) {
                return false;
            }
        }
        return true;
    }

    isEdgeExists(source, target) {
        const isStraightEdgeExists = this.cy.elements(`edge[source = '${source}'][target = '${target}']`).length > 0
        const isBackEdgeExists = this.cy.elements(`edge[source = '${target}'][target = '${source}']`).length > 0
        return isStraightEdgeExists || isBackEdgeExists;
    }
}

const holder = new Holder(new Store(), api);

export default holder;
