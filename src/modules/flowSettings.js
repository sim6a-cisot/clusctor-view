class FlowSettings {
    priority = 30000;
    idleTimeout = 60;
    hardTimeout = 0;
    ethType = 'ipv4';
    ipProto = '';
    servicePort = '';
}

export {FlowSettings};