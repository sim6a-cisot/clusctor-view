const SegmentationStep = Object.freeze({
    PRIMARY_SEGMENTS: 'primarySegments',
    SECONDARY_SEGMENTS: 'secondarySegments',
    MERGING: 'merging'
});

export { SegmentationStep };