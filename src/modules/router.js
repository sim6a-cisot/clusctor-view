import TopologyPage from '../components/main/MainPage';
import ToolsPage from '../components/tools/ToolsPage';
import SettingsPage from '../components/settings/SettingsPage';
import StatisticsPage from '../components/statistics/StatisticsPage';
import RoutesPage from "../components/routes/RoutesPage";

import VueRouter from 'vue-router';
import Vue from 'vue';


Vue.use(VueRouter);

const routes = [
    {path: '/', component: TopologyPage},
    {path: '/tools', component: ToolsPage},
    {path: '/settings', component: SettingsPage},
    {path: '/statistics', component: StatisticsPage},
    {path: '/routes', component: RoutesPage}
];

const router = new VueRouter({routes: routes});

export default router;
