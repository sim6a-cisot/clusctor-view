
class Host {
    constructor(name, mac, ip) {
        this.name = name;
        this.mac = mac;
        this.ip = ip;
    }
}

class Collection {
    constructor(hosts) {
        this.hosts = hosts;
    }

    findByName(name) {
        const filtered = this.hosts.filter(host => host.name === name);
        if (filtered.length !== 1) {
            return '';
        }
        return filtered[0]
    }
}

export { Host, Collection };